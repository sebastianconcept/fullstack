import Layout from "../components/Layout";
import Header from "../components/Header";
import Intro from "../components/Intro";
import Story from "../components/Story";
import CoolImages from "../components/CoolImages";
import CoolIdeas from "../components/CoolIdeas";
import Testimonials from "../components/Testimonials";
import Social from "../components/Social";

export default class Main extends React.Component {
  render() {
    return (
      <Layout>
        <Header />
        <div className="main-content">
          <div className={"content-container wow fadeIn"}>
            <Intro />
            {/* <CoolCode></CoolCode> */}
            {/* <LetsTalk></LetsTalk> */}
            <Story />
          </div>
          <CoolImages />
          <CoolIdeas />
          <Testimonials />
          <Social />
          <section id="footer" className={"wow fadeInUp"}>
            <h2>
              <p className="section-title">
                <span>{new Date().getFullYear()}</span>
              </p>
            </h2>
          </section>
        </div>
      </Layout>
    );
  }
}
