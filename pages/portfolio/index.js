import Layout from "../../components/Layout";
import InnerNavBar from "../../components/InnerNavBar";
import Portfolio from "../../components/Portfolio";

export default class Main extends React.Component {
  render() {
    return (
      <Layout>
        <InnerNavBar />
        <div className="main-content">
          <div className={"content-container wow fadeIn"}>
            <Portfolio />;
          </div>
        </div>
      </Layout>
    );
  }
}