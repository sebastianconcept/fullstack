import Layout from "../../components/Layout";
import InnerNavBar from "../../components/InnerNavBar";
import Techs from "../../components/Techs";
import ScheduleCall from "../../components/ScheduleCall";

export default class Main extends React.Component {
  render() {
    return (
      <Layout>
        <InnerNavBar />
        <div className="cv-container">
          <section id="CV">
            <h1>Sebastian Sastre</h1>
            <div className="bio-oneliner">
              <p>
                Senior Software Engineer with a background at Telna, Sulvo,
                StarterSquad, Airflowing and a 5-star rating on Codementor.
              </p>
            </div>
            <div className="contact-info">
              <span>sebastianconcept@gmail.com</span>
              <span> +55 (45) 98421-2515</span>
              <a href="https://sebastiansastre.co/"> Website</a> |{" "}
              <a href="https://blog.sebastiansastre.co/">Blog</a> |{" "}
              <a href="https://www.linkedin.com/in/sebastiansastre/">
                LinkedIn
              </a>{" "}
              | <a href="https://github.com/sebastianconcept">GitHub</a>
            </div>
            {/* <div className="cv-bio">
              In the last 9 years successfully delivered remote full-stack
              software development work helping diverse teams in companies and
              startups scaling their products.
            </div> */}
            {/* <div className="tech-stacks">
              <h3>Tech Stacks</h3>
              <div className="">
                <ul className="stacks">
                  <li className="tech">Smalltalk</li>
                  <li className="tech">.</li>
                  <li className="tech">.</li>
                </ul>
              </div>
            </div> */}
            <h3 className="cv-section">Work Experience</h3>
            <ul className="experience">
              <li className="experience-item">
                <h4>Senior Software Engineer</h4>
                <span> Telna, Sep 2019 - Aug 2023</span>
                <ul className="milestones">
                  <li className="milestone">
                    Joined a team of 6 as a Senior Software Engineer to keep the
                    12 engineers operations team and 50 member customer success
                    teams running and escalating the main component of the
                    roaming software. This resulted in 2022 driving 1 Petabyte
                    of network traffic in the mobile north-american
                    telecommunications market.
                  </li>
                  <li className="milestone">
                    In 6 months I've refactored the main backend code to improve
                    Separations of Concerns issues that were a blocker for
                    containerization of the service. As a result of that,
                    continuous deployment and automated scaling and improved
                    monitoring became possible.
                  </li>
                  <li className="milestone">
                    In 2 days, implemented a proof of concept to prove we could
                    migrate from Voyage to{" "}
                    <a href="https://blog.sebastiansastre.co/article/mapless-is-online-again">
                      Mapless
                    </a>{" "}
                    to introduce significant improvements in the backend. After
                    demonstrating this PoC to the Principal Telecommunications
                    Architect and CTO and getting them impressed, it got
                    approved and fully implemented in 3 months for 2 out of 3
                    protocols. That year I've got promoted to Senior Software
                    Engineer and the service continued to grow.
                  </li>
                  <li className="milestone">
                    Made and demonstrated a Rust caching system to extend the
                    this microservice capacity which is currently above 25Kops
                    and growing. Although this application is not in production
                    at this time, its discoveries allowed other caching
                    improvements increasing system capacity by alleviating
                    significantly the DB usage.
                  </li>
                  <li className="milestone">
                    Made a Golang multithreaded client to produce synthetic
                    network traffic for stress testing the backend making
                    possible to discover a virtual machine issue that the open
                    source community and external consultants are resolving and
                    the safe limits of each instance in operation.
                  </li>
                </ul>
                <Techs
                  techs={[
                    "Smalltalk",
                    "Rust",
                    "Golang",
                    "Python",
                    "bash",
                    "TDD",
                    "Docker",
                    "SvelteKit",
                    "NextJs",
                    "Tailwind",
                  ]}
                />
              </li>
              <li className="experience-item">
                <h4>Lead Software Engineer</h4>
                <span> Sulvo, May 2017 - Dec 2018</span>
                <ul className="milestones">
                  <li className="milestone">
                    As the team lead at Sulvo, working directly with the
                    founder, I was responsible for designing and implementing
                    the next generation ad server in a JavaScript-based stack. I
                    collaborated with a team of five software engineers to
                    ensure a smooth transition from the old system to the new
                    one. I designed and implemented the operations for the new
                    ad server and successfully migrated it from AWS to GCloud
                    without any service disruption. In addition to my technical
                    contributions, I also helped the founder hire three new
                    engineers and led the team to migrate back from GCloud to
                    AWS without any loss of data or functionality, despite
                    handling billions of transactions globally. My leadership
                    and technical expertise helped Sulvo establish a
                    next-generation ad server with seamless migration between
                    cloud platforms and a skilled team to support and enhance
                    it.
                  </li>
                  {/* <li className="milestone">
                    Scored 92% in{" "}
                    <a href="https://www.coursera.org/account/accomplishments/certificate/TTEZAZDQ59B6">
                      ML course
                    </a>
                  </li> */}
                </ul>
                <Techs
                  techs={[
                    "AWS",
                    "GCloud",
                    "JavaScript",
                    "Meteor",
                    "VueJS",
                    "NodeJS",
                    "MongoDB",
                    "Docker",
                    "Machine Learning",
                  ]}
                />
              </li>
              <li className="experience-item">
                <h4>Full-stack Software Engineer</h4>
                <span>StartedSquad, Apr 2015 - May 2016</span>
                <ul className="milestones">
                  <li className="milestone">
                    In just one week, using Meteor, I developed an MVP for two
                    founders of a dairy industry startup in the Netherlands. The
                    MVP was designed to solve matching online demand and offer
                    of cows in the dairy industry, and as a result of its
                    success, later the founders were able to secure a Series A
                    funding of €4M.
                  </li>
                  <li className="milestone">
                    As a full-stack developer, I successfully developed and
                    published a Swift native app in the App Store for a client.
                    Additionally, I developed the app's NodeJS backend and Vue
                    dashboard frontend, creating a complete solution that helped
                    the client offer a seamless experience to his customers.
                  </li>
                  <li className="milestone">
                    As a frontend developer, I joined 4 teams ranging from 2 to
                    12 developers, working on tasks using AngularJS and ReactJS.
                    Notably, I mentored junior developers from Vandebron.nl in
                    using AngularJS to develop new features for their platform
                    in the energy sector. Additionally, I worked with
                    cross-functional teams to complete frontend tasks using both
                    AngularJS and ReactJS, ultimately improving the user
                    experience and completing tasks weekly in a timely and
                    efficient manner.
                  </li>
                </ul>
                <Techs
                  techs={[
                    "Swift",
                    "ReactJS",
                    "Meteor",
                    "CircleCI",
                    "AngularJs",
                  ]}
                />
              </li>

              {/* <li className="experience-item">
                <h4>Open Source Developer</h4>
                <span> 2014 - 2015</span>
                <ul className="milestones">
                  <li className="milestone">
                    Published{" "}
                    <a href="https://github.com/flow-stack/flow">flow-stack</a>{" "}
                    framework (MIT)
                  </li>
                  <li className="milestone">
                    Published{" "}
                    <a href="https://github.com/sebastianconcept/Mapless">
                      Mapless
                    </a>{" "}
                    Object Persistence framework (MIT)
                  </li>
                </ul>
                <Techs techs={["Smalltalk", "jQuery", "JavaScript"]} />
              </li> */}

              {/* <li className="experience-item">
                <h4>Independent consultant</h4>
                <span> 2013 - 2014</span>
                <ul className="milestones">
                  <li className="milestone">
                    Web app for the telecommunications industry.
                  </li>
                  <li className="milestone">
                    Helped in a back-office frontend application for{" "}
                    <a href="https://www.zorgdomein.nl/">ZorgDomein</a> in a
                    team of 15-20 developers.
                  </li>
                  <li className="milestone">
                    Helped Freiburg IT Systems to implement a back-office system
                    for <a href="https://www.pitztal.com/en">Pitztal</a>’s CRM
                    using Seaside.
                  </li>

                  <li className="milestone">
                    Completed Design Thinking Lab from Stanford University and
                    Human-Computer Interaction from San Diego University.
                  </li>
                </ul>
                <Techs
                  techs={[
                    "Credit Card Processing",
                    "Smalltalk",
                    "Seaside",
                    "AngularJS",
                    "Design Thinking",
                  ]}
                />
              </li> */}

              <li className="experience-item">
                <h4>Flowing</h4>
                <span>Jul 2009</span>
                <ul className="milestones">
                  <li className="milestone">
                    <a href="https://pharo.org/success/AirFlowing.html">
                      Airflowing
                    </a>{" "}
                    Small ad agencies had no system other than email and excel
                    to keep the company organized in these days. After
                    demonstrating an MVP to 8 ad agencies owners of the local
                    commerce group (ACIFI), Airflowing was developed into a full
                    software as a service (SaaS) product for end-to-end team
                    management focused on medium/small businesses. It was
                    designed to scale horizontally in the cloud in the days
                    before the cloud was called the cloud. Airflowing helped
                    clients to take control of sales, operations and cashflow.
                    In its launch it received praise from Guy Kawasaki, Seth
                    Godin and Derek Sivers Airflowing’s own clients.
                  </li>
                </ul>
                <Techs
                  techs={[
                    "Smalltalk",
                    "Seaside",
                    "Object Oriented Database",
                    "jQuery",
                    "Marketing",
                    "UX/UI Design",
                    "Product Design",
                    "Business Development",
                    "MongoDB",
                    "Proprietary Frameworks",
                    "Public Speaking",
                  ]}
                />
              </li>

              <li className="experience-item">
                <h4>Galenial Startup</h4>
                <span> Nov 2007 - Nov 2008</span>
                <ul className="milestones">
                  <li className="milestone">
                    After seeing physicians operating small clinics and
                    organizing them with docs on Word and Excel, found
                    experimenting the market potential by developing an MVP.
                    Galenial was a product to aid in the administration of small
                    clinics and medical practice management developed using
                    Dolphin Smalltalk. Learned how to implement probabilistic
                    search based on a Berkeley's paper (Cheshire II) and the
                    process to deploy a full client-server product in Windows
                    for two clinics.
                  </li>
                </ul>
                <Techs
                  techs={[
                    "Smalltalk",
                    "Crystal Reports",
                    "Object Oriented Database",
                    "Non-boolean search",
                  ]}
                />
              </li>

              <li className="experience-item">
                <h4>Independent consultant</h4>
                <span>Feb 2003 - Dec 2005</span>
                <ul className="milestones">
                  <li className="milestone">
                    Designed and implemented a Windows based desktop e-commerce
                    application for{" "}
                    <a href="http://www.cromosol.com/">Cromosol S.A.</a> the
                    Argentina's market leader of auto parts aftermarket in 2
                    months. This application successfully expanded all their
                    distribution channels cross country and made ordering over
                    fax obsolete.
                  </li>
                </ul>
                <Techs
                  techs={[
                    "Smalltalk",
                    "Crystal Reports",
                    "Object Oriented Database",
                    "MySQL",
                    "PostgresSQL",
                  ]}
                />
              </li>

              <li className="experience-item">
                <h4>Project Leader</h4>
                <span> TEING Nov 1998 - Dec 2000</span>
                <ul className="milestones">
                  <li className="milestone">
                    In my first job, joined a team of 4 software engineers to
                    develop the next-gen client-server toll system using Dolphin
                    Smalltalk and PostgreSQL to replace the old inflexible
                    PLC-based system. Promoted to Project Leader after
                    successfully installing the first version of the developed
                    system in production.
                  </li>
                </ul>
                <Techs
                  techs={[
                    "Smalltalk",
                    "Crystal Reports",
                    "PostgresSQL",
                    "Proprietary Hardware",
                  ]}
                />
              </li>
            </ul>

            <div className="computer-languages">
              <h3>Computer Languages</h3>
              <ul className="milestones">
                <li className="milestone">Smalltalk.</li>
                <li className="milestone">JavaScript.</li>
                <li className="milestone">Swift.</li>
                <li className="milestone">Rust.</li>
                <li className="milestone">Golang.</li>
                <li className="milestone">Python.</li>
              </ul>
            </div>
            <div className="languages">
              <h3>Languages</h3>
              <ul className="milestones">
                <li className="milestone">Fluent english.</li>
                <li className="milestone">Fluent portuguese.</li>
                <li className="milestone">Native spanish.</li>
              </ul>
            </div>
          </section>

          <p className="curiosity">
            What are the engineering challenges your team is currently tackling?
          </p>
          <ScheduleCall />
          <section id="footer" className={"wow fadeInUp"}>
            <h3 className="section-title" style={{ fontSize: "1.2em" }}>
              {new Date().getFullYear()}
            </h3>
          </section>
        </div>
      </Layout>
    );
  }
}
