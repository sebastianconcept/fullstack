export default class CoolIdeas extends React.Component {
  render () {
    return (
      <section id='cool-ideas' className={'wow fadeIn'}>
        <h2 className='title-without-subtitle'>
          <p className='section-title'>
            <span>I think these ideas are seriously cool</span>
          </p>
        </h2>
        <div className='cool-ideas-container'>
          <a
            href='https://www.youtube.com/watch?v=oKg1hTOQXoY'
            target='blank();'
          >
            The Computer Revolution Hasn't Happened yet
          </a>

          <a
            href='https://www.youtube.com/watch?v=gTAghAJcO1o'
            target='blank();'
          >
            The Future Doesn't Have to Be Incremental
          </a>

          <a
            href='http://www.laputan.org/mud/mud.html#BigBallOfMud'
            target='blank();'
          >
            Big Ball of Mud
          </a>

          <a
            href='https://en.wikipedia.org/wiki/Separation_of_concerns'
            target='blank();'
          >
            Separation of Concerns
          </a>

          <a
            href='https://www.youtube.com/watch?v=Ao9W93OxQ7U'
            target='blank();'
          >
            Object-Oriented Programming
          </a>

          <a
            href='https://www.youtube.com/watch?v=pW-SOdj4Kkk'
            target='blank();'
          >
            Preventing the Collapse of Civilization
          </a>

          <a
            href='https://www.youtube.com/watch?v=agOdP2Bmieg'
            target='blank();'
          >
            The Humane Representation of Thought
          </a>

          <a
            href='https://www.youtube.com/watch?v=hTIk9MN3T6w'
            target='blank();'
          >
            Why is Consciousness so Mysterious?
          </a>

          <a
            href='https://www.youtube.com/watch?v=JJ0FQR2gXe0'
            target='blank();'
          >
            Leading by Omission
          </a>
        </div>
      </section>
    )
  }
}
