import ReactGA from "react-ga";
import Head from "next/head";

const isServer = typeof window === "undefined";
const WOW = !isServer ? require("wow.js") : null;

export default class Layout extends React.Component {
  componentDidMount() {
    new WOW().init({ offset: 300 });
    ReactGA.initialize("G-N10SJH351X");
    ReactGA.pageview("/home");
  }

  render() {
    return (
      <div>
        <Head>
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,700;0,900;1,400&family=Roboto+Slab:wght@200&family=Share+Tech+Mono&display=swap"
            rel="stylesheet"
          />
          <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css"
          />
          <link rel="stylesheet" href="/static/style.css" />
        </Head>
        {this.props.children}
      </div>
    );
  }
}
