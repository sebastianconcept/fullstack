export default function Techs({ techs }) {
  return (
    <div className="tech-names">
      {techs.map((e, i) => {
        if (techs.length > i + 1) {
          return <span key={i} className="tech-name">{e}, </span>;
        } else {
          return <span key={i} className="tech-name">{e}</span>;
        }
      })}
    </div>
  );
}
