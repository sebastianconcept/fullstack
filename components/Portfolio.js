import StoryEpic from "./StoryEpic";
import ScheduleCall from "./ScheduleCall";
export default class Portfolio extends React.Component {
  render() {
    return (
      <section>
        <h2 id="story-title" className="title-without-subtitle">
          <p className="section-title">
            <span>Portfolio</span>
          </p>
        </h2>
        <p className="portfolio-intro">
          Here's a glimpse of projects I've contributed to
        </p>
        <StoryEpic
          reason="Full stack SaaS MVP"
          title="Proposto"
          date="December 2022"
          image="/static/img/propostoSquare1EN.png"
          imageUrl="https://proposto.co"
          text="Published product page for lead capture for an MVP of a SaaS. Tech stack: Tailwind CSS on SvelteKit for optimal page load and SEO and SSR plus a Pharo backend. The product page and entire web application is properly responsive in all major platforms."
        />
        <StoryEpic
          reason="Native mobile and full-stack development"
          title="AppStore iOS Swift App and Admin webapp"
          date="January 2017"
          image="/static/img/apito.gif"
          text="Published a native iOS application. For this particular app it was required Swift for the iOS app and the following frameworks: MapKit, FBSDKLoginKit, SwiftyJSON, ToastSwiftFramawork, Async and Bolts. And for its admin web app, it was used Meteor with SemanticUI."
        />
        <StoryEpic
          reason="Open-sourced full-stack framework"
          title="flow"
          date="July 2014"
          image="/static/img/flow.gif"
          imageUrl="https://github.com/flow-stack/flow"
          text="Published flow as an open-source project with a mission. Flow's mission is to provide consultants, startups and software houses with a competitive Smalltalk full-stack framework that allows them to quickly deliver a demo with all the modern html5."
        />
        <StoryEpic
          reason="Open-sourced persistence framework"
          title="Mapless"
          date="March 2014"
          image="/static/img/mapless.gif"
          imageUrl="https://sebastianconcept.github.io/Mapless/"
          text="Published Mapless as an open-source project. Persistence with very low maintenance. For real. No instVars. No getters and no setters - just models that can be saved. Mapless is a persistence framework with zero Object-Relational-Impedance-Mismatch on your data."
        />
        <StoryEpic
          reason="Hospitality Industry"
          title="Pitztal"
          image="/static/img/pitztal.jpg"
          imageUrl="https://www.pitztal.com/en#Featured"
          date="February 2013"
          text="Lead conversion for back-office processing with seaside web app. Integrated the API of a highly customized CRM with a Seaside based web app and the admin dashboard to process hundreds of leads for the hospitality industry in this region."
        />
        <StoryEpic
          reason="Being a startup founder"
          title="Airflowing"
          date="July 2009"
          image="/static/img/airflowing.jpg"
          imageUrl="https://www.youtube.com/watch?v=7Z29q5PUoJs"
          text="Airflowing was a web application that provided management to service companies. With an original UI design and simplicity in mind, it allowed the operations team to coordinate jobs, tasks and finances across different teammates. It was a simple yet end-to-end solution for small businesses. It ran on Pharo Smalltalk, jQuery, Aggregates, OmniBase object persistence achieving scalable and subsecond server-side rendering speeds. Shutdown in 2015."
        />
        <StoryEpic
          reason="Feature out of a Berkeley's paper"
          title="Implementing Cheshire II"
          date="July 2008"
          image="/static/img/galenial.jpg"
          imageUrl="http://cheshire.berkeley.edu/"
          text="Developed a product for medical practice management including functions that were quite an innovation at the time like efficient video capture of various medical instruments used in different exams. Also organizing the medical records storage with probabilistic indexing based on U.C. Berkeley's Cheshire II catalog design, a feature that allowed sub-second non-boolean searches and results presented in order of relevance. All that developed using Dolphin Smalltalk."
        />
        <style jsx>{`
          #story-title {
            margin-left: 2em;
            margin-right: 2em;
          }
          .portfolio-intro {
            text-align: center;
            margin-bottom: 6rem;
            font-size: 2em;
          }
        `}</style>
        <p className="inviting">
          I'm confident I can add a lot of value to your team
        </p>
        <ScheduleCall />
        <section id="footer" className={"wow fadeInUp"}>
          <h3 className="section-title" style={{ fontSize: "1.2em" }}>
            {new Date().getFullYear()}
          </h3>
        </section>

        <style jsx>{`
          .inviting {
            margin-top: 2em;
            font-size: 1.3em;
            font-weight: 500;
            font-style: italic;
            text-align: center;
          }
        `}</style>
      </section>
    );
  }
}
