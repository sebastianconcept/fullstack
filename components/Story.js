import StoryEpic from "./StoryEpic";
export default class Story extends React.Component {
  render() {
    return (
      <section id="story">
        <h2 id="story-title" className="title-without-subtitle">
          <p className="section-title">
            <span>Here is my developer story</span>
          </p>
        </h2>
        {/* <div className='stories-title'>
          <h3 className='story-reason'>Raison d'être</h3>
          <div>
          </div>
          <h3 className='milestone-title'>Milestone</h3>
        </div> */}
        <StoryEpic
          reason="Telecomunications"
          title="Senior software engineer"
          date="November 2021"
          image="/static/img/telna2022.png"
          imageUrl="https://telna.com"
          text="Joined a team of 4 as a Senior Software Engineer to oversee the operations of a 12-member engineering team and a 50-member customer success team, while also managing the main component of the roaming software. This effort led to a notable achievement in 2022, contributing to the management of nearly 1 Petabyte of network traffic in the North American mobile telecommunications market."
        />
        <StoryEpic
          reason=""
          title="Backend software engineer"
          date="September 2019"
          image="/static/img/telna.jpg"
          imageUrl="https://telna.com"
          text="Engaged in full-time collaboration with the mobile network infrastructure team. One notable aspect of the software being developed here is its commitment to maintaining the utmost standards of stability and availability, while also retaining a foundation of adaptability. This strategic approach is of paramount importance, given the necessity to seamlessly integrate diverse networks across the globe as required. The expectation for code quality remains exceedingly high, with a focus on delivering pristine code in every pull request and master merge."
        />
        <StoryEpic
          reason="Machine Learning"
          title="ML Certificate of Accomplishment"
          date="July 2017"
          image="/static/img/ml-certificate.jpg"
          imageUrl="https://www.coursera.org/account/accomplishments/certificate/TTEZAZDQ59B6"
          text="Graded 92% on this online non-credit course authorized by Stanford University and offered through Coursera. We've covered Linear Regression, Logistic Regression, Regularization to prevent overfitting during training, Neural Networks, Support Vector Machines, Unsupervised Learning, Anomaly Detection, Recommender Systems and  Photo OCR. The final work of this course was training a network to control the wheel of a vehicle to make it self-drive following the road."
        />
        <StoryEpic
          reason="Upgrading a production system in hot and in a massive scale"
          title="Backend compatible lossless upgrade"
          date="May 2017"
          image="/static/img/lossless.jpg"
          text="Helped to design and implement the migration process of a dynamically scalable service from AWS to GCloud. Once the service was migrated to GCloud, the upgrades where backward compatible and lossless. The clients started to consume the new API version organically and the new API servers got globally balanced adding server instances and collapsing the deprecated ones as required."
        />
        <StoryEpic
          reason="Ad-tech in high scale"
          title="Full-stack developer at Sulvo"
          date="May 2017"
          image="/static/img/sulvo-logo.png"
          text="Architected and implemented the next-generation app that is serving +1.2 billion  transactions per month. Part of the innovation team making features and dealing with the challenges that add value to this platform used by premium users in the Ad industry. Full-stack with JavaScript (ES6) and DevOps using CloudFlare, CI, Docker, GCloud and AWS."
        />

        <StoryEpic
          reason="Native mobile and full-stack development"
          title="AppStore iOS Swift App and Admin webapp"
          date="January 2017"
          image="/static/img/apito.gif"
          text="Published a native iOS application. For this particular app it was required Swift for the iOS app and the following frameworks: MapKit, FBSDKLoginKit, SwiftyJSON, ToastSwiftFramawork, Async and Bolts. And for its admin web app, it was used Meteor with SemanticUI."
        />
        <StoryEpic
          reason="Native mobile development"
          title="CS193P, Stanford University"
          date="March 2016"
          image="/static/img/stanford_logo.jpg"
          text="Learned the basics of Swift by learning to develop apps on iOS. This allowed me to release an app in the AppStore which used MapKit, FacebookSDK, JSON, multithreading and low-latency sound."
        />
        <StoryEpic
          reason="Encouraging the startup culture among smalltalkers"
          title="Smalltalk for Startups"
          date="November 2014"
          image="/static/img/smalltalks2014.gif"
          imageUrl="https://www.youtube.com/watch?v=iumCpezWzkU"
          text="A talk at the Smalltalks 2014 meetup to link the Startup opportunities with Smalltalk opportunities. Also, flow's public presentation and demo."
        />
        <StoryEpic
          reason="Open-sourced full-stack framework"
          title="flow"
          date="July 2014"
          image="/static/img/flow.gif"
          imageUrl="https://github.com/flow-stack/flow"
          text="Published flow as an open-source project with a mission. Flow's mission is to provide consultants, startups and software houses with a competitive Smalltalk full-stack framework that allows them to quickly deliver a demo with all the modern html5."
        />
        <StoryEpic
          reason="Open-sourced persistence framework"
          title="Mapless"
          date="March 2014"
          image="/static/img/mapless.gif"
          imageUrl="https://sebastianconcept.github.io/Mapless/"
          text="Published Mapless as an open-source project. Persistence with very low maintenance. For real. No instVars. No getters and no setters - just models that can be saved. Mapless is a persistence framework with zero Object-Relational-Impedance-Mismatch on your data."
        />
        <StoryEpic
          reason="Lots of frontend and some full-stack development"
          title="Full-stack developer at StarterSquad"
          date="January 2014"
          image="/static/img/startersquad.gif"
          imageUrl="https://www.startersquad.com"
          text="At StarterSquad worked in various teams and projects with many stacks. Among them Angular, Meteor, ExpressJS, MongoDB, PostgreSQL, RESTful APIs, Swift and flow."
        />
        <StoryEpic
          reason="Design Thinking"
          title="Design Thinking Action Lab"
          image="/static/img/designThinking.gif"
          imageUrl="https://www.class-central.com/course/novoed-design-thinking-action-lab-919"
          date="July 2013"
          text="I gained valuable insights through my experience with Leticia Britos Cavagnaro during Stanford's Design Thinking Action Lab. Initially intended to enhance the startup creation and innovation stages, I discovered its significance extended beyond that scope. It emerged as a crucial asset, particularly in guiding the Customer Development and Prototyping phases."
        />
        <StoryEpic
          reason="Hospitality Industry"
          title="Pitztal"
          image="/static/img/pitztal.jpg"
          imageUrl="https://www.pitztal.com/en#Featured"
          date="February 2013"
          text="Lead conversion for back-office processing with seaside web app. Integrated the API of a highly customized CRM with a Seaside based web app and the admin dashboard to process hundreds of leads for the hospitality industry in this region."
        />
        <StoryEpic
          reason="Volunteering"
          title="TEDxAvCataratas"
          image="/static/img/tedx.jpg"
          imageUrl="https://www.ted.com/tedx/events/8928"
          date="Jan 2012"
          text="I was involved in leading, organizing, fundraising, creating partnerships and coordinating volunteers and providers to make the first TEDx event in the incredible Iguassu Falls. The event was sold-out."
        />
        <StoryEpic
          reason="Being a startup founder"
          title="Airflowing"
          date="July 2009"
          image="/static/img/airflowing.jpg"
          imageUrl="https://pharo.org/success/AirFlowing"
          text="Airflowing was a web application that provided management to service companies. With an original UI design and simplicity in mind, it allowed the operations team to coordinate jobs, tasks and finances across different teammates. It was a simple yet end-to-end solution for small businesses. It ran on Pharo Smalltalk, jQuery, Aggregates, OmniBase object persistence achieving scalable and subsecond server-side rendering speeds. Shutdown in 2015."
        />
        <StoryEpic
          reason="Feature out of a Berkeley's paper"
          title="Implementing Cheshire II"
          date="July 2008"
          image="/static/img/galenial.jpg"
          imageUrl="http://cheshire.berkeley.edu/"
          text="Developed a product for medical practice management including functions that were quite an innovation at the time like efficient video capture of various medical instruments used in different exams. Also organizing the medical records storage with probabilistic indexing based on U.C. Berkeley's Cheshire II catalog design, a feature that allowed sub-second non-boolean searches and results presented in order of relevance. All that developed using Dolphin Smalltalk."
        />
        <StoryEpic
          reason="Client-server architecture in the aftermarket industry"
          title="Ford parts e-commerce"
          date="July 2006"
          image="/static/img/sla_cromosol.jpg"
          imageUrl="http://www.cromosol.com"
          text="As an independent consultant, developed an e-commerce application for Cromosol S.A., a leader in the Argentinean cars and trucks aftermarket. The application had a very simple to use user interface, designed to help Cromosol’s clients to see always updated prices with their stock availability and send parts requests."
        />
        <StoryEpic
          reason="Road toll system"
          title="Toll system"
          date="July 2001"
          image="/static/img/toll.jpg"
          imageUrl="http://www.en.teing.com.ar/index.html"
          text="Project Leader at TEING, an industrial control systems company. The developed product was a Smalltalk based system to manage and control toll processing in a Road Concession Company. The software-controlled custom hardware devices like barriers, alarms, beacons, fiscal printers, hubs, axle counters and more. In those industrial environments, getting parts of the system offline due to thunderbolts is a common occurrence, so the system also had high-performance requirements with several fault-tolerant strategies that allowed the system to operate in real-time and also with the server offline. All that running on Dolphin Smalltalk."
        />

        <style jsx>{`
          #story-title {
            margin-left: 2em;
            margin-right: 2em;
          }
        `}</style>
      </section>
    );
  }
}
