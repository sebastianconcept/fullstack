import NavBar from "./NavBar";
export default class Header extends React.Component {
  render() {
    return (
      <section id="header">
        <NavBar />
        <div className="header-text">
          <div className="hey-hi">
            <div></div>
            <div className="hey-hi-parts">
              <h4 className="hello">
                <p>Hi, I'm</p>
              </h4>
              <h1 className="main-name">Sebastian Sastre</h1>
              <h4 className="hello">
                <p>I make computers behave</p>
              </h4>
            </div>
            <div></div>
          </div>
        </div>
        <img className="main-image" src="/static/img/hi.jpg" />
      </section>
    );
  }
}
