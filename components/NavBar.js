export default class NavBar extends React.Component {
  scrollTo(event, emelentId) {
    document.querySelector(`#${emelentId}`).scrollIntoView({
      behavior: "smooth",
    });
  }

  render() {
    return (
      <div
        className={"nav-bar-wrapper wow fadeInLeft"}
        data-wow-duration="0.5s"
      >
        <div className="nav-bar">
          <a onClick={(event) => this.scrollTo(event, "header")}>Top</a>
          <a href="//blog.sebastiansastre.co">Blog</a>
          <a onClick={(event) => this.scrollTo(event, "story")}>Story</a>
          <a onClick={(event) => this.scrollTo(event, "cool-ideas")}>Ideas</a>
          <a onClick={(event) => this.scrollTo(event, "social")}>Social</a>
          <a href="/portfolio">Portfolio</a>
          <a href="/cv">CV</a>
          {/* <a onClick={(event)=>this.scrollTo(event,'cool-code')}>Code</a> */}
        </div>
      </div>
    );
  }
}
