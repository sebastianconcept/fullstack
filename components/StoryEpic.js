import Techs from "./Techs";

export default class StoryEpic extends React.Component {
  render() {
    let image;
    if (this.props.imageUrl && this.props.image) {
      image = (
        <div className="story-image">
          <a href={this.props.imageUrl} target="blank();">
            <img src={this.props.image}></img>
          </a>
        </div>
      );
    } else if (this.props.image) {
      image = (
        <div className="story-image">
          <img src={this.props.image}></img>
        </div>
      );
    } else {
      image = <div className="story-image"></div>;
    }

    return (
      <div className="story-epic">
        <div className="story-reason">
          <h4>{this.props.reason}</h4>
        </div>
        {image}
        <div className="story-point">
          <h3>{this.props.title}</h3>
          <div className="story-epic-date">{this.props.date}</div>
          <p>{this.props.text}</p>
          {!!this.props.techs && <Techs techs={this.props.techs} />}
        </div>
      </div>
    );
  }
}
