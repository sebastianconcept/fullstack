
export default class Image extends React.Component{
  render(){
    return (
      <div className='thumbnail wow fadeInUp'>
      <style jsx>{`
        .thumbnail {
          background-image: url(${this.props.src});
          background-size: cover;
        }
      `}</style>
      </div>
    )
  }
}
