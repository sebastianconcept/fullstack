export default class NavBar extends React.Component {
  scrollTo(event, emelentId) {
    document.querySelector(`#${emelentId}`).scrollIntoView({
      behavior: "smooth",
    });
  }

  render() {
    return (
      <div
        className={"nav-bar-wrapper wow fadeInLeft"}
        data-wow-duration="0.5s"
      >
        <div className="nav-bar">
          <a href="/">Home</a>
        </div>
      </div>
    );
  }
}
