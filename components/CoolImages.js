import Image from './Image'

export default class CoolImages extends React.Component{

  render(){
    return (
      <section id='cool-images'>
        <h2 className='title-without-subtitle'>
          <p className='section-title'><span>Pictures I like</span></p>
        </h2>
        <div className='images-container'>
        <Image src='/static/img/amazon.jpg'/>
        <Image src='/static/img/term.jpg'/>
        <Image src='/static/img/500.jpg'/>

        <Image src='/static/img/vegetables.jpg'/>
        <Image src='/static/img/primeRib1.jpg'/>
        {/* <Image src='/static/img/primeRib2.jpg'/> */}
        {/* <Image src='/static/img/palito.jpg'/> */}
        {/* <Image src='/static/img/bananaIcecream.jpg'/> */}
        <Image src='/static/img/icecream.jpg'/>


        <Image src='/static/img/feria.jpg'/>
        {/* <Image src='/static/img/building.jpg'/>
        <Image src='/static/img/ba1.jpg'/>
        <Image src='/static/img/ba2.jpg'/> */}
        {/* <Image src='/static/img/ba3.jpg'/> */}
        <Image src='/static/img/ba4.jpg'/>

        <Image src='/static/img/horus.jpg'/>
        <Image src='/static/img/ring.jpg'/>
        <Image src='/static/img/sunset1.jpg'/>
        {/* <Image src='/static/img/sunset2.jpg'/>
        <Image src='/static/img/sunset3.jpg'/>
        <Image src='/static/img/sunset4.jpg'/>
        <Image src='/static/img/sunset5.jpg'/> */}

        {/* <Image src='/static/img/becomeTheBook.jpg'/> */}
        <Image src='/static/img/question.jpg'/>
        {/* <Image src='/static/img/cantRefuse.jpg'/> */}

        <Image src='/static/img/flower1.jpg'/>
        <Image src='/static/img/trees.jpg'/>
        <Image src='/static/img/tucano.jpg'/>
        <Image src='/static/img/poetCat.jpg'/>

        {/* <Image src='/static/img/essay2.jpg'/> */}
        {/* <Image src='/static/img/vascularity.jpg'/> */}
        <Image src='/static/img/essay1.jpg'/>
        {/* <Image src='/static/img/itaipu1.jpg'/> */}
        {/* <Image src='/static/img/itaipu2.jpg'/> */}
        {/* <Image src='/static/img/itaipu3.jpg'/> */}
        {/* <Image src='/static/img/itaipu4.jpg'/> */}
        {/* <Image src='/static/img/itaipu5.jpg'/> */}
        {/* <Image src='/static/img/xenomorph.jpg'/> */}
        {/* <Image src='/static/img/vf1.jpg'/> */}
        {/* <Image src='/static/img/vf12.jpg'/> */}
        <Image src='/static/img/thinking.jpg'/>
        <Image src='/static/img/onlyChoices.jpg'/>
        {/* <Image src='/static/img/natureFindsAWay.jpg'/> */}
        <Image src='/static/img/obe.jpg'/>
        </div>
      </section>
    )
  }
}