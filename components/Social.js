import ScheduleCall from "./ScheduleCall";
export default class Social extends React.Component {
  render() {
    return (
      <section id="social" className={"wow fadeInUp"}>
        <h2>
          <p className="section-title">
            <span>Connecting on social media</span>
          </p>
        </h2>
        <div className="section-subtitle">
          We can continue the conversation on any of these
        </div>
        <div className="social-media-icons">
          <a
            href="https://www.linkedin.com/in/sebastiansastre/"
            target="blank();"
          >
            <img src="/static/img/LinkedIn.png" alt="Linked In" />
          </a>
          <a href="http://github.com/sebastianconcept" target="blank();">
            <img src="/static/img/github.png" alt="GitHub" />
          </a>
          <a href="https://instagram.com/sebastianconcept/" target="blank();">
            <img src="/static/img/Instagram.png" alt="Instagram" />
          </a>
        </div>
        <p className="inviting">
          I'm confident I can add a lot of value to your team
        </p>
        <ScheduleCall />
        <style jsx>{`
          .inviting {
            margin-top: 2em;
            font-size: 1.3em;
            font-weight: 500;
            font-style: italic;
            text-align: center;
          }
        `}</style>
      </section>
    );
  }
}
