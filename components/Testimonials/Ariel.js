import Image from '../Image'
export default class Ariel extends React.Component {
  render () {
    return (
      <div className='testimonial-case'>
        <div className='reference-image-container testimonial-image'>
          <Image src={'/static/img/ariel.jpeg'} />
        </div>
        <div className='testimonial-text'>
          <p>
            Sebastian is the kind of Software Engineer you don't find often. He
            is very organized in his work, writes amazing code and really
            delivers.
          </p>
          <p>
            I learned a lot from him while working together, especially in
            architecting and writing highly readable code. His great attention
            to details makes him someone you can completely trust for tasks that
            require high precision.
          </p>
          <p>
            He is also a true team player with excellent communication skills,
            which makes him a very valuable asset to any team.
          </p>

          <p>
            <strong>Ariel Fuggini</strong>
            <br /> Helping publishers maximize revenue through technology and
            innovation
          </p>
        </div>

        <style jsx>{`
          .testimonial-case {
            margin-bottom: 4em;
          }
        `}</style>
      </div>
    )
  }
}
