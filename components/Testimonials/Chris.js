import Image from '../Image'
export default class Chris extends React.Component {
  render () {
    return (
      <div className='testimonial-case'>
        <div className='reference-image-container testimonial-image'>
          <Image src={'/static/img/chris.jpeg'} />
        </div>
        <div className='testimonial-text'>
          <p>
          During my time as a Core IP Engineer at Telna I worked as part of a small yet deeply talented team of consummate technical professionals, and without hesitation consider Sebastian as having held up his part to count among that number.
          </p>
          <p>
          Anytime we had to deal with insanely tight build deadlines and tough problems, he was there with us.
          </p>
          <p>
          Seb could be always be depended on to help us work his angle of whatever challenges we faced through to their resolution - even under sometimes extremely stressful conditions.
          </p>
          <p>
            Over my 25-year career, he is one of those few people I can honestly say I miss working with everyday. He would be a valued and highly regarded member of any team he finds himself on.
          </p>
          <p>
            <strong>Chris Aspeling</strong>
            <br /> Head of Architecture and Development @ The Starter Pack Company
          </p>
        </div>

        <style jsx>{`
          .testimonial-case {
            margin-bottom: 4em;
          }
        `}</style>
      </div>
    )
  }
}
