import Image from '../Image'
export default class Vlad extends React.Component {
  render () {
    return (
      <div className='testimonial-case'>
        <div className='reference-image-container testimonial-image'>
          <Image src={'/static/img/vlad.jpeg'} />
        </div>
        <div className='testimonial-text'>
          <p>
            It is my pleasure to recommend Sebastian Sastre for the position of
            software developer / project manager / CTO. I worked as Sebastian’s
            project manager within StarterSquad for the last 5 years, during
            which time we took on many medium and large software projects.
          </p>
          <p>
            Sebastian is a great developer and a wonderful teammate. During the
            many projects I had the opportunity to work with him, he always
            added a creative angle to the technical solution, was easy to work
            with and responsible. He is well-liked by our clients and respected
            by his colleagues.
          </p>
          <p>
            Sebastian has a passion for coding and new technologies, even
            writing / contributing to software frameworks, giving lectures at
            various software community events.
          </p>

          <p>
            His energy is limitless and his enthusiasm serves to motivate
            everyone on his team. I’m confident that you will be very pleased
            with his work, should you choose to involve him with your company /
            projects. Please feel free to contact me if you have questions,
            either by email: vlad@startersquad.com or by phone: 0040 742 301
            902.
          </p>

          <p>
            Sincerely, <br />
            <br />
            <strong>Vlad Ioanitescu</strong>
            <br /> Project Manager
            <br /> StarterSquad BV
            <br />
            COO at Seamless Ahead
          </p>
        </div>

        <style jsx>{`
          .testimonial-case {
            margin-bottom: 6em;
          }
        `}</style>
      </div>
    )
  }
}
