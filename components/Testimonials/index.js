import Chris from './Chris'
import Vlad from './Vlad'
import Ariel from './Ariel'
import Juan from './Juan'
export default class Testimonials extends React.Component {
  render () {
    return (
      <section id='testimonials' className={'content-container wow fadeInUp'}>
        <h2>
          <p className='section-title'>
            <span>Testimonials</span>
          </p>
        </h2>
        <div className='section-subtitle'>
          This is the privilege of working with great teammates
        </div>
        <Chris />
        <Ariel />
        <Vlad />
        {/* <Juan /> */}
        <style jsx>{`
          #testimonials {
            // margin: 0 4em;
          }
        `}</style>
      </section>
    )
  }
}
