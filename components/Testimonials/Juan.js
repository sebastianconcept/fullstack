import Image from '../Image'
export default class Juan extends React.Component {
  render () {
    return (
      <div className='testimonial-case'>
        <div className='reference-image-container testimonial-image'>
          <Image src={'/static/img/juan.jpeg'} />
        </div>
        <div className='testimonial-text'>
          <p>
            Sebastian is as much as a Design Thinker as a doer. He is fearless
            about innovation and focuses a lot in the user experience. I’ve saw
            him lead the team that organised the first TEDx in the Iguassu Falls
            which was a flawless success.
          </p>
          <p>
            I also saw him doing software from the backend all the way up to the
            front-end. Clean code and sound concepts. His nonsense radar is
            awesome. Talking with him is always insightful and inspiring. I can
            talk with him for hours from design and marketing to databases and
            networks to psychology and anthropology.
          </p>
          <p>
            If you want a team of inspired rockstars you should talk with him!
            He is also an awesome human being.
          </p>

          <p>
            <strong>Juan Bernabó</strong>
            <br /> Management, Innovation & Startups Expert
            <br /> Pragmatic Management Approach Author
            <br /> Germinadora Founder
          </p>
        </div>

        <style jsx>{`
          .testimonial-case {
            margin-bottom: 6em;
          }
        `}</style>
      </div>
    )
  }
}
