import Image from '../Image'

export default class ReferenceSlide extends React.Component {
  render () {
    return (
      <div className='reference-slide'>
        <div className='reference-image-container'>
          <Image src={this.props.src} />
        </div>
        <div className='reference-text-section'>
          <p className='reference-text'>{this.props.text}</p>
          <h3 className='reference-name'>{this.props.name}</h3>
          <p className='reference-bio'>{this.props.bio}</p>
        </div>
        <style jsx>{`
          p {
            margin-top: 0;
          }

          .reference-slide {
            display: grid;
            grid-template-columns: minmax(200px, 25%) auto;
          }

          .reference-text-section {
            margin-left: 3em;
            margin-right: 3em;
          }

          p.reference-text {
            font-size: 1.3em;
            font-weight: 500;
            font-style: italic;
            text-align: left;
          }

          .reference-name {
            margin-bottom: 0.2em;
          }

          .reference-bio {
            color: var(--soft);
            font-weight: 700;
            font-size: 0.8em;
            margin-bottom: 2em;
          }

          /* tablets and desktop */
          @media only screen and (min-width: 768px) and (max-device-width: 1024px) {
            .reference-image-container {
              margin: 0 auto;
            }

            .reference-slide {
              grid-template-columns: 100%;
            }

            .reference-text-section {
              margin-top: 3em;
            }

            p.reference-text {
              font-size: 1.3em;
            }

            h3.reference-name {
              font-size: 1.3em;
            }

            .reference-bio {
              font-size: 1em;
            }
          }
        `}</style>
      </div>
    )
  }
}
