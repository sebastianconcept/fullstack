import Carousel from 'react-multi-carousel'
import 'react-multi-carousel/lib/styles.css'
import ReferenceSlide from './ReferenceSlide'

export default class References extends React.Component {
  static getInitialProps ({ req, isServer }) {
    let userAgent
    let deviceType
    if (req) {
      userAgent = req.headers['user-agent']
    } else {
      userAgent = navigator.userAgent
    }
    const md = new MobileDetect(userAgent)
    if (md.tablet()) {
      deviceType = 'tablet'
    } else if (md.mobile()) {
      deviceType = 'mobile'
    } else {
      deviceType = 'desktop'
    }
    return { deviceType }
  }
  state = { isMoving: false }

  render () {
    const responsive = {
      desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 1,
        slidesToSlide: 1
      },
      tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 1,
        slidesToSlide: 1
      },
      mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        slidesToSlide: 1
      }
    }

    return (
      <Carousel
        /*
          swipeable={false}
          draggable={false}
          */
        arrows
        responsive={responsive}
        ssr
        swipeable
        showDots
        infinite
        autoPlay
        autoPlaySpeed={10000}
        keyBoardControl
        // customLeftArrow={<SlideLeftArrow />}
        beforeChange={() => this.setState({ isMoving: true })}
        afterChange={() => this.setState({ isMoving: false })}
        containerClass='first-carousel-container container'
        deviceType={this.props.deviceType}
      >
        <ReferenceSlide
          src='/static/img/nik.jpeg'
          name='Nikolaus Graf'
          bio='Consultant / Workshops / Freelance focusing on GraphQL (Schema Design, Processes, Development) & React (Development)'
          text='Sebastian is a product focused engineer who has a solid understanding of both, frontend as well as backend. He cares about the user experience and knows a lot about various UX patterns. Working with him was always a pleasure and I can highly recommend that you do too.'
        />
        <ReferenceSlide
          src='/static/img/ivana.jpeg'
          name='Ivana Sendecka'
          bio='Change manager, project manager and leadership development in corporate, NGO and national governing body'
          text='Sebastian is a remarkable design thinker. He can combine a software engineering approach with an emotionally intelligent one, which is really unique. It is always inspiring to exchange ideas with him. His generous spirit is reflected by adding TEDx curation to his busy creative life. Work with him and see yourself.'
        />
        <ReferenceSlide
          src='/static/img/paul.jpg'
          name='Paul Wilke'
          bio='Software engineer at Freiburg Systems'
          text="Sebastian really gets into the head of the user. He delivers the solution that fits not only by developing regarding to specs, but by figuring out what it takes to deliver a great UX. It was a pleasure working with him and I really learned a lot from him. He's a not just a developer he is a design thinker and on top of that a great person."
        />
        <ReferenceSlide
          src='/static/img/esteban.jpeg'
          name='Esteban Maringolo'
          bio='Senior Software Developer'
          text='Sebastian is one of the few developers with strong skills both in programming as in user interaction. His focus on frictionless experiences is the foundation for his work and life. He is always in the verge searching for the perfect flow.'
        />
        {/* <ReferenceSlide
          src='/static/img/sayan.jpeg'
          name='Sayan Mukhopadhyay'
          bio='Machine learning | Big Data | High Performance Computing | Cloud'
          text='Seb is very passionate about whatever he does. His wide bandwidth in technical knowledge and ability to learn new things can make an inseparable member of any organization. He is very open and helping in nature. I will recommend him for any position which required technical excellence with a good heart.'
        /> */}
        <ReferenceSlide
          src='/static/img/juliane.jpeg'
          name='Juliane Kosiak Poitevin'
          bio='Marketing e Comunicação no Hospital Santa Cruz e Paraná Clínicas - Curitiba'
          text='There are many brand and operational details to take care in Cataratas S.A. and Iguassu National Park. I saw Sebastian articulate every one of our requirements successfully along with the ones of the TEDx itself. All with enthusiasm and professionalism. Hosting the first TEDx in town in such special place as the Iguassu Falls was memorable for all of us and Sebastian and his partners and team made us glad of our decision to be part of it.'
        />
        <ReferenceSlide
          src='/static/img/rafa.jpeg'
          name='Rafael Laynes'
          bio='Executivo de vendas Sr na RPC - Rede Paranaense de Comunicação'
          text="Everybody wants to do something that triggers changes in society, which highlights Sebastian is that he not only wants. He does. We worked together creating partnerships and getting TEDxCataratas funded. We faced adversity together, and I saw that nothing puts him down. The result of this effort was a TEDx born in quality in a very special place. Excellent ideas to share. I'll work side by side on Sebastian's projects anytime."
        />
        {/* <style jsx>{`

        `}</style> */}
      </Carousel>
    )
  }
}
