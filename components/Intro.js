import ScheduleCall from "./ScheduleCall";
import References from "./References";

class Intro extends React.Component {
  render() {
    return (
      <div id="intro" className={"wow fadeIn"}>
        <div className="intro-content">
          {/* <h2>
            <p className='section-title no-after about-me'>
              A little bit about me
            </p>
          </h2> */}
          <p>
            I’m a <strong>Senior Software Engineer</strong> with a bias towards
            writing flexible and elegant code to build systems that operation
            teams can trust and scale and developers find easy to read and
            maintain.{" "}
            <a
              href="https://en.wikipedia.org/wiki/Technical_debt"
              target="blank();"
            >
              I'm good at keeping tech-debt at bay
            </a>
            .
          </p>

          <p>
            <strong>
              +{new Date().getFullYear() - 1 - 2012}/y remote work experience
            </strong>{" "}
            in many different teams. I've worked with teammates from Australia,
            South America, eastern and western Europe, Asia but mostly the U.S.
            and Canada.
          </p>
          <p>
            <strong>
              +{new Date().getFullYear() - 1 - 1999}/y total in software
              engineering
            </strong>{" "}
            strong at OOP used in a functional fashion.
          </p>

          <h2 className="title-without-subtitle ">
            <p className="section-title no-after">
              <span>Some remarks</span>
            </p>
          </h2>

          {/* <h2 id='remarks'>
            <p className='section-title no-after'>Some remarks</p>
          </h2> */}
          <ul>
            <li>
              <strong>I’ve architected and coded</strong> software that serves
              billions transactions per month impacting millions of users in the
              telecomunications and digital ads industries.
            </li>
            <li>
              I’ve set up a backend update process that did a lossless hot
              upgrade keeping backward compatibility while serving thousands of
              transactions per second.
            </li>
            <li>
              I've taken the{" "}
              <a href="http://cheshire.berkeley.edu/" target="blanck();">
                Cheshire II U.C. Berkeley's paper
              </a>{" "}
              on <i>probabilistic search</i> (non-boolean) and implemented it as
              a software feature in a product.
            </li>
            <li>
              <strong>I've open-sourced</strong> a{" "}
              <a
                href="https://github.com/sebastianconcept/flow"
                target="blanck();"
              >
                full-stack
              </a>{" "}
              framework and a{" "}
              <a
                href="https://github.com/sebastianconcept/mapless"
                target="blanck();"
              >
                persistence
              </a>{" "}
              framework, both available in{" "}
              <a href="https://github.com/sebastianconcept/" target="blank()">
                my GitHub
              </a>{" "}
              account.
            </li>
            <li>
              <strong>I've open-sourced</strong>{" "}
              <a
                href="https://github.com/sebastianconcept/merchant"
                target="blanck();"
              >
                Merchant
              </a>
              , an API client to perform{" "}
              <a
                href="https://www.pcicomplianceguide.org/faq/#1"
                target="blanck();"
              >
                PCI compliant
              </a>{" "}
              e-commerce transactions also available in my GitHub account.
            </li>
          </ul>
          <p>
            Check more interesting stories in my{" "}
            <a href="#story">developer story</a> below.
          </p>
        </div>
        <div className="intro-references">
          <h2 id="references" className="title-without-subtitle ">
            <p className="section-title no-after">
              <span>What they say about me</span>
            </p>
          </h2>

          <References />
        </div>
        <p className="curiosity">
          What are the challenges you're currently facing in engineering?
        </p>
        <ScheduleCall />
        <div className="intro-keywords">
          <h2 className="title-without-subtitle ">
            <p className="section-title no-after">
              <span>I have familiarity with</span>
            </p>
          </h2>

          {/* <h2 id='familiarity'>
            <p className='section-title no-after'>I have familiarity with</p>
          </h2> */}
          <p className="keywords">
            Object Oriented Programming, Functional-Programming, JavaScript,
            ECMAScript-6, Smalltalk, Rust, Swift, XCode, NodeJS, ExpressJS,
            React, Redux, Vue, Vuex, Angular, Meteor, iOS, macOS, ubuntu, nginx,
            Apache, MongoDB, PostgreSQL, Docker, Docker-compose,
            Continuous-Integration, AWS, ElasticBeanstalk, Lambda, GCloud,
            CloudFlare, CloudFlare Workers, Agile, Design Thinking and Machine
            Learning.
          </p>
        </div>
        <style jsx>{`
          .intro-keywords {
            margin-top: 4em;
          }

          @media only screen and (min-width: 768px) and (max-device-width: 1024px) {
            #references {
              margin-left: 3em;
              margin-right: 3em;
            }

            .title-without-subtitle {
              margin-top: 4em;
              margin-bottom: 2em;
            }

            p {
              // font-size: 1.4em;
            }
          }
        `}</style>
      </div>
    );
  }
}

export default Intro;
